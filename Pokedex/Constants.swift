//
//  Constants.swift
//  Pokedex
//
//  Created by Wally Contreras on 7/16/17.
//  Copyright © 2017 Wally Contreras. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"


//*Closure
typealias DownloadComplete = () -> ()
